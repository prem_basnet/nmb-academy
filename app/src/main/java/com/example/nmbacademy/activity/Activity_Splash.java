package com.example.nmbacademy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.nmbacademy.R;


public class Activity_Splash extends AppCompatActivity {

    public static int SPLASH_TIME_OUT=3000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(getApplicationContext(), Activity_BottomNav.class);
                startActivity(i);
                //overridePendingTransition(R.anim.anim_slide_out_right,R.anim.anim_slide_in_right);
                finish();
            }
        },SPLASH_TIME_OUT);
    }
}
