package com.example.nmbacademy.activity;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.nmbacademy.R;
import com.example.nmbacademy.fragment.Fragment_Dashboard;
import com.example.nmbacademy.fragment.Fragment_Feedback;

public class Activity_BottomNav extends AppCompatActivity {

    //private TextView mTextMessage;
    BottomNavigationView navView;
    FragmentTransaction tx;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            Fragment fragment = null;

            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                  //  mTextMessage.setText(R.string.menu_dashboard);
                    fragment = new Fragment_Dashboard();
                    break;
                case R.id.navigation_notifications:
                   // mTextMessage.setText(R.string.menu_notifications);

                    return true;
                case R.id.navigation_feedback:
                   // mTextMessage.setText(R.string.menu_settings);
                    fragment = new Fragment_Feedback();
                    break;
                case R.id.navigation_profile:
                   // mTextMessage.setText(R.string.menu_contact_us);
                    return true;
            }
            return loadFragment(fragment);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_nav);

        navView = findViewById(R.id.nav_view);
       // mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        loadFragment(new Fragment_Dashboard());
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();
            return true;
        }
        return false;
    }

}
