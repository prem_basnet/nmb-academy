package com.example.nmbacademy.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.nmbacademy.R;
import com.example.nmbacademy.activity.Activity_Trainers;
import com.example.nmbacademy.activity.Activity_Training;
import com.example.nmbacademy.adapter.ImageSliderAdapter;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import me.relex.circleindicator.CircleIndicator;

public class Fragment_Dashboard extends Fragment {

    View rootView;
    ImageView img_training,img_trainers;
    LinearLayout training,trainers;
    TextView txtTraining,txtTrainers;

    private long delayTime =  5000;
    private Context context;
    private AutoScrollViewPager viewPager;
    RecyclerView recyclerView;
    CircleIndicator pageIndicatorView;
    ImageSliderAdapter adapterImageSlider;
    int images[] = {R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4};
    String titles[] = {"Fernando Torres (£50 million), from Liverpool to Chelsea in 2011",
            "Kepa Arrizabalaga joined the Blues for a record-breaking fee of £72m",
            "Alvaro Morata (£60 million), from Real Madrid to Chelsea in 2017"
            , "Familiarize yourself with the real test format of IELTS."};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);

        context = getContext();

        pageIndicatorView = (CircleIndicator) rootView.findViewById(R.id.pageIndicator);
        viewPager = (AutoScrollViewPager) rootView.findViewById(R.id.viewPager);
        img_training = rootView.findViewById(R.id.training_image);
        img_trainers = rootView.findViewById(R.id.trainer_image);
        training = rootView.findViewById(R.id.training_layout);
        trainers = rootView.findViewById(R.id.trainer_layout);
        txtTraining = rootView.findViewById(R.id.training_text);
        txtTrainers = rootView.findViewById(R.id.trainers_text);
        //  recyclerView = (RecyclerView) rootView.findViewById(R.id.home_recycler_view);

        training.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getContext(), Activity_Training.class);
                startActivity(in);
            }
        });

        trainers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getContext(), Activity_Trainers.class);
                startActivity(in);
            }
        });

        img_training.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getContext(), Activity_Training.class);
                startActivity(in);
            }
        });

        img_trainers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getContext(), Activity_Trainers.class);
                startActivity(in);
            }
        });

        txtTraining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getContext(), Activity_Training.class);
                startActivity(in);
            }
        });

        txtTrainers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getContext(), Activity_Trainers.class);
                startActivity(in);
            }
        });

        viewPager.startAutoScroll();
        viewPager.setInterval(delayTime);
        viewPager.setStopScrollWhenTouch(true);

        adapterImageSlider = new ImageSliderAdapter(context,images,titles);
        viewPager.setAdapter(adapterImageSlider);
        pageIndicatorView.setViewPager(viewPager);


        return rootView;
    }


    @Override
    public void onPause() {
        viewPager.stopAutoScroll();
        super.onPause();
    }

    @Override
    public void onResume() {
        viewPager.startAutoScroll();
        viewPager.setInterval(delayTime);
        super.onResume();
    }
}
