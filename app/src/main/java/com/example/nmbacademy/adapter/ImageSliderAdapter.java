package com.example.nmbacademy.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.example.nmbacademy.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ADMIN on 2017-12-29.
 */

public class ImageSliderAdapter extends PagerAdapter {

    private Context context;
    int images[];
    String titles[];
   // List<FeaturedNewsReceiveParams.FeaturednewsBean> featurednewsBeen = new ArrayList<>();
    private LayoutInflater layoutInflater;

    public ImageSliderAdapter(Context context, int images[], String titles[]){
        this.context = context;
        this.images = images;
        this.titles = titles;
        //this.featurednewsBeen = featurednewsBeen;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.image_slider, container, false);

        ImageView imageView = (ImageView) view.findViewById(R.id.featured_image);
        TextView txtTitle = (TextView) view.findViewById(R.id.featured_title);
        imageView.setImageResource(images[position]);
        txtTitle.setText(titles[position]);

     /*  final FeaturedNewsReceiveParams.FeaturednewsBean pos = featurednewsBeen.get(position);
        if(featurednewsBeen.size() > 0) {

            Picasso.with(context)
                    .load(pos.getImage())
                    .error(R.drawable.no_image)
                    .placeholder(R.drawable.placeholder)
                    .into(imageView);

            txtTitle.setText(Html.fromHtml(pos.getTitle()));
            txtDate.setText(Html.fromHtml(MyApplication.convertDate(pos.getDate())));

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(v.getContext(),Activity_FeaturedNews_Description.class);
                    intent.putExtra("obj",pos);
                    Activity activity = (Activity) context;
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.anim_slide_out_right,R.anim.anim_slide_in_right);
                }
            });
        }*/

        ((ViewPager)container).addView(view);
        return view;
    }

    @Override
    public int getCount() {
       return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}
